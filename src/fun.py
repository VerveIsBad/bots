import discord
from discord.ext import commands
import random
import asyncio

class Fun(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(description = "Spam command only for fun.")
    @commands.has_role('Verve')
    async def spam(self, ctx,message:str = None, target:discord.Member = None, loop:int = 10):
        for i in range(loop):
            await ctx.send(f"{message} {target.mention}")
            await asyncio.sleep(0.5)


    @commands.command(description = "MMMM")
    async def override(self, ctx, member:discord.Member = None):
        dead = discord.utils.get(ctx.guild.roles, name = "Dead")
        if member != None:
            await member.remove_roles(dead)
            embed = discord.Embed(
                title = "Un-deaded",
                description = f"{member.mention} has been un-deaded",
                color = 0x00FF00
            )
            await ctx.send(embed=embed)
        else:
            for user in (await ctx.guild.fetch_members().flatten()):
                if user.has_role(dead):
                    user.remove_roles(dead)
                else:
                    pass


    @commands.command(description = "Get your or another users avatar.")
    async def avatar(self, ctx, member:discord.Member = None):
        if member != None:
            image = member.avatar_url
        else:
            image = ctx.message.author.avatar_url
        await ctx.send(image)

    @commands.command(description = 'Kicks yourself from the server.')
    async def suicide(self, ctx):
        '''
        Kicks yourself from the server.
        '''
        member:discord.Member = ctx.message.author
        embed = discord.Embed(
            title = "Oops...",
            description = f"damn, looks like {ctx.author} took a ride on the suislide...",
            color = 0xFF0000
        )
        await ctx.send(embed=embed)
        await member.kick(reason = "Suicde.")

    @suicide.error
    async def suicide_error(self, ctx: commands.Context, error: commands.CommandError):
        
        if isinstance(error, commands.BadArgument):
            await ctx.send(embed = discord.Embed(title = '!!! BAD ARGUMENTS !!!', description = f'Please check your arguments and try again. (you cannot suicide other users, only yourself.) Error: {error}'))

    @commands.command(description = 'IQ level of you or a given user.')
    async def iq(self, ctx, target:discord.Member = None):
        '''
        IQ level of you or a given user.
        '''
        if target == None:
            await ctx.send(f"Your IQ is {random.randint(-10, 330)}!")
        else:
            await ctx.send(f'{target.mention}\'s IQ is {random.randint(-10, 330)}!')
        
    @commands.command(description = 'Flip a coin!')
    async def flip(self, ctx):
        '''
        Flip a coin!
        '''
        chance = random.randint(0,1)
        if chance == 1:
            await ctx.send('Heads!!')
        else:
            await ctx.send("Tails!!")

    @commands.command(description = "Make me flop, or flop on a specified user!")
    async def flop(self,ctx, target:discord.Member = None):
        '''
        Make me flop, or flop on a specified user!
        '''
        if target != None:
            await ctx.send(f'*flops on {target.mention}* mmm nice and soft ~~')
        else:
            await ctx.send(f'*flops* <~ w~>')
    
    @commands.command()
    async def father(self, ctx):
        await ctx.send("My father is @Verve UwU")

    @commands.command(description = 'Ask if im cute, or a specified user!')
    async def cute(self, ctx, target:discord.Member = None):
        '''
        Ask if im cute, or a specified user!
        '''
        if target != None:
            if random.randint(0,1) == 1:
                await ctx.send(f'{target.mention} is cute!!!! *pats*')
            else:
                await ctx.send(f"Eh, {target.mention} aint that cute >.>")
        else:
            await ctx.send(random.choice(cute_replies))

    @commands.command(description = 'Snuggles a  targeted user!')
    async def snuggle(self, ctx, target:discord.Member = None):
        '''
        Snuggles a  targeted user!
        '''
        await ctx.send(f'Awww~~ you snuggle {target.mention} with love and effection UwU')

    @commands.command(description = 'Gay \% of you or a targeted user.')
    async def gay(self, ctx, target = None):
        '''
        created by BlueJake. (friendo)
        '''
        if target == None:
            await ctx.send(f'You are {random.randint(0,100)}% gay!')
        else:
            await ctx.send(f'{target} is {random.randint(0,100)}% gay!')

    @commands.command(description = 'Cuddles a targeted user!')
    async def cuddle(self, ctx, target:discord.Member = None):
        '''
        Cuddles a targeted user!
        '''
        await ctx.send(f'Aww, You cuddle {target.mention} softly~ UwU')

    @commands.command(description = 'Kill yourself or a specific target!!')
    async def kill(self, ctx, target:discord.Member = None):
        '''
        Kill yourself or a specific target!!
        '''
        if target != None:
            await ctx.send(f'*gasp* you killed {target.mention}!!! 0-0')
        else:
            await ctx.send(f'you stabbed yourself, how odd.')

    @commands.command(description = 'pp size of your or a user!')
    async def pp(self, ctx, target:discord.Member = None):
        '''
        pp size of your or a user!
        '''
        x = ['=' for i in range(random.randint(0, 50))] # generate the pp
        x[0] = 'B'
        x[len(x)-1] = 'D'
        string = ''.join(x)

        if target == None:
            await ctx.send(f'Your pp size is {string}')
        else:
            await ctx.send(f'{target.mention}\'s pp size is {string}')

    @commands.command(description = 'Hugs a user!!')
    async def hug(self, ctx, target:discord.Member = None):
        '''
        Hugs a user!!
        '''
        await ctx.send(f'Aww, you hug {target} tightly!!~')

    @commands.command(description = 'Praise a given user!')
    async def praise(self, ctx, target:discord.Member = None):
        '''
        Praise a given user!
        '''
        await ctx.send(f'Prasie the allmighty {target.mention}!! *bows*')

    @commands.command(description = "Dom \% of you or a given user!")
    async def dom(self, ctx, target:discord.Member = None):
        '''
        "Dom \% of you or a given user!"
        '''
        if target == None:
            await ctx.send(f"You are {random.randint(0, 100)}% a dom!")
        else:
            await ctx.send(f"{target.mention} is {random.randint(0, 100)}% a dom!")
    
    @commands.command(description = "Get the sub \% of you or a user.")
    async def sub(self, ctx, target:discord.Member = None):
        '''
        Get the sub % of you or a user.
        '''
        if target == None:
            await ctx.send(f"Your {random.randint(0, 100)}% a sub!")
        else:
            await ctx.send(f'{target.mention} is {random.randint(0, 100)}% a sub!')


