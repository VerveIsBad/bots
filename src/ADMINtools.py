import discord
from discord.ext import commands
import random
import asyncio


class AdminTools(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(description = "Mutes a specified user.")
    @commands.has_role('Admin') 
    async def mute(self, ctx, target:discord.Member = None, time:int = 0, time_type:str = "s"): 
        ot = time
        Muted = discord.utils.get(ctx.guild.roles, name = 'Muted')
        if Muted not in target.roles: # not muted 
            await target.add_roles(Muted)
            if time == 0: # no defined time
                embed = discord.Embed(
                    title = "User muted", 
                    description = f"User {target.mention} is muted.",
                    color = 0x00FF00
                )
                await ctx.send(embed = embed)
            else: 
                time, time_type = self.get_time(time, time_type) # get the time 
                if time < 1: # if time error (time can only be 1+, if its below 1 its a error)
                    error = discord.Embed( # create error message 
                        title = "!!! TIME ERROR !!!",
                        description = "Invalid time for time. Please use types: s, m, h, d (seconds, minutes, hours, days) in the format of: ```$<command> [target] <time> [time_type]```",
                        color = 0xFF0000 
                    )
                    await ctx.send(embed = error)
                else: # not time error 
                    user_muted = discord.Embed(
                        title = "User muted.",
                        description = f"User {target.mention} has been muted for {ot} {time_type}",
                        color = 0x00FF00
                    )
                    await ctx.send(embed = user_muted)
                    await asyncio.sleep(time)
                    unmute = discord.Embed( # embed message
                        title = f'User unmuted!', 
                        description = f'User {target.mention} has been unmuted.',
                        color = 0x00FF00
                    )
                    await target.remove_roles(Muted) # unmute 
                    await ctx.send(embed = unmute)
        else:
            error = discord.Embed( # user already muted.
                title = '!!! ERROR !!!', 
                description = f'User is already muted!',
                color = 0xFF0000
            )
            await ctx.send(embed = error)

    @commands.command(description = "Unmutes a specified user.")
    @commands.has_role('Admin') 
    async def unmute(self, ctx: commands.Context, member: discord.Member = None):
        '''
        Unmutes a specified user.
        $unmute <user @>
        '''
        muteRole = discord.utils.get(ctx.guild.roles, name = 'Muted')
        memberRole = discord.utils.get(ctx.guild.roles, name = 'meh followers')

        if muteRole in member.roles:
            await member.remove_roles(muteRole)
            await member.add_roles(memberRole)
            embed = discord.Embed(
                title = "unmute", 
                description = f'Unmuted - {member.mention}',
                color = 0x00FF00
            )
            await ctx.send(embed = embed)
        else:
            embed = discord.Embed(
                title = '!!! ERROR !!!', 
                description = f"{member.mention} is not muted!",
                color = 0xFF0000
            )
            await ctx.send(embed = embed)
    
    @commands.command(description = "Make the targeted user an admin")
    @commands.has_role('Admin')
    async def adminify(self, ctx: commands.Context, member: discord.Member):
        Admin = discord.utils.get(ctx.guild.roles, name = "Admin")

        embed = discord.Embed(
            title = f"User has been adminified.",
            description = f"User {member.mention} has been made a admin.",
            color = 0x00FF00
        )
        await member.add_roles(Admin)
        await ctx.send(embed = embed)

    @commands.command(description = "Unadmin a user")
    @commands.has_role('Admin')
    async def unadminify(self, ctx: commands.Context, member: discord.Member):
        Admin = discord.utils.get(ctx.guild.roles, name = 'Admin')

        if Admin not in member.roles:
            embed = discord.Embed(
                title = "!!! USER NOT AN ADMIN !!!",
                description = f"User {member.mention} does not have the 'Admin' role.",
                color = 0xFF0000
            )
            await ctx.send(embed = embed)
        else:
            embed = discord.Embed(
                title = "User unadminified",
                description = f"User {member.mention} has been unadminifed.",
                color = 0x00FF00
            )
            await member.remove_roles(Admin)
            await ctx.send(embed = embed)

    @commands.command(description = 'Kick a given user.')
    @commands.has_role('Admin')
    async def kick(self, ctx: commands.Context, member: discord.Member, *, reason = None):
        '''
        Kick a given user.
        $kick <user @>
        '''
        embed = discord.Embed(
            title = f'kicked user {member}', 
            description = f'Reason: {reason}',
            color = 0x00FF00
        )
        await ctx.send(embed = embed)
        embed = discord.Embed(
            title = "Kicked from the server.", 
            description = f"You have been kicked from the server. Reason: {reason}",
            color = 0xFF0000
        )
        await member.send(embed = embed)
        await member.kick(reason=reason)

    @commands.command(description = 'ban a given user.')
    @commands.has_role('Admin')
    async def ban(self, ctx: commands.Context, member: discord.Member, *, reason = None):
        '''
        ban a given user.
        '''
        embed = discord.Embed(
            title = f'{member.mention} has been banned.', 
            description = f"Reason: {reason}",
            color = 0x00FF00
        )
        await ctx.send(embed = embed)
        embed = discord.Embed(
            title = f"Banned from the server.",
            description = f"You have been banned from the server. Reason: {reason}",
            color = 0xFF0000
        )
        await member.ban(reason = reason)

    @commands.command(description = 'Add a role to given user.')
    @commands.has_role('Admin')
    async def addRole(self, ctx: commands.Context, target: discord.Member = None, roleName = None):
        '''
        Add a role to given user
        '''
        role = discord.utils.get(ctx.guild.roles, name = roleName)
        await target.add_roles(role)
        embed = discord.Embed(
            title = "ROLE GIVEN", 
            description = f'Role: {role} given to {target.mention}',
            color = 0x00FF00
        )
        await ctx.send(embed = embed)

    @commands.command(description = 'Remove a role from given user.')
    @commands.has_role('Admin')
    async def delRole(self, ctx: commands.Context, target: discord.Member = None, roleName = None):
        '''
        Delete a role from a given user.
        '''
        role = discord.utils.get(ctx.guild.roles, name = roleName)
        await target.remove_roles(role)
        embed = discord.Embed(
            title = "ROLE REMOVED", 
            description = f'Role: {role} removed from {target.mention}',
            color = 0x00FF00
        )
        await ctx.send(embed = embed)

    @commands.command(description = "Deletes x amount of messages from a given user.")
    @commands.has_role('Admin')
    async def purge(self, ctx:commands.Context, member:discord.Member = None, limit:int = None):
        
        if limit == None:
            limit = 20
        elif limit > 50:
            limit = 50
        else:
            pass
        total = 0
        messages = await ctx.channel.history(limit=50).flatten()
        for message in messages:
            if message.author == member:
                await message.delete()
                total += 1
                if total == limit:
                    break 
                else:
                    continue
            else:
                continue
        embed = discord.Embed(
            title = "Messages deleted.",
            description = f"{total} messages have been deleted from {member.mention}",
            color = 0x00FF00
        )
        await ctx.send(embed = embed)
        


    @commands.Cog.listener()
    async def on_command_error(self, ctx: commands.Context, error: commands.CommandError):
        if isinstance(error, commands.BadArgument):
            
            embed = discord.Embed(
                title = '!!! BAD ARGUMENTS !!!', 
                description = f'Please check your arguments and try again. Error: {error}',
                color = 0xFF0000
            )
            return await ctx.send(embed = embed)

        elif isinstance(error, commands.MissingRole):
            
            embed = discord.Embed(
                title = '!!! MISSING PERMISIONS !!!', 
                description = f'You do not have permission to use this command.',
                color = 0xFF0000
                )
            return await ctx.send(embed = embed)

        else:
            embed = discord.Embed (
                title = "!!! ERROR !!!",
                description = f"A unknown error has occured. This error has been logged and will be looked into shortly. (Error: {error} )",
                color = 0xFF0000
            )
            await ctx.send(embed=embed)
            with open("unkown_errors.txt", "a") as f:
                f.write(f"{ctx} | {error}\n")
                return
    
    def get_time(self, time:int, time_type:str = "s"):
        '''
        input | 5s | 5 * 1 | 5m | 5 = 5 * 60. | 5h | 5 * 3600 | 5d | 5 * 86400 | 
        seconds * 1
        minutes * 60
        hours * 3600
        days * 86400
        '''
        if time_type == 's' or time_type == "seconds": # convert time shorthands
            time_type = "seconds"
        elif time_type == "m" or time_type == "minutes": # also account for full bames
            time_type = "minutes"
        elif time_type == "h" or time_type == "hours":
            time_type = "hours"
        elif time_type == "d" or time_type == "days":
            time_type = "days"
        else:
            return -1
            
        table = { # times table
            "seconds": 1,
            "minutes": 60,
            "hours":   3600,
            "days":    86400
        }
        return time * table[time_type], time_type # return a tuple with new time and ime type


    

